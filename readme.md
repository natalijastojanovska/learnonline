# Learn Online
Welcome to Learn Online! I made this project while learning HTML, CSS and Bootstrap. 

## How to use this?

If you want to try this app on your local machine, you need to clone this repository on your local environment by following this command: 
```git clone https://gitlab.com/natalijastojanovska/learnonline.git```
and then open the file named index.html


